function toggleSidebarMenu() {
  window.$('#dismiss, .overlay, .close-button').on('click', function () {
    window.$('.sidebar-container').removeClass('active');
    window.$('.overlay').removeClass('active');
  });

  window.$('#sidebarCollapse').on('click', function () {
    window.$('.sidebar-container').addClass('active');
    window.$('.overlay').addClass('active');
    window.$('.collapse.in').toggleClass('in');
  });
}